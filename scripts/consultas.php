<!DOCTYPE html>
<html lang="es">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <link rel="stylesheet" type="text/css" href="../materialize/materialize.css">
    <title>Web Juan - Alex, Sara E Ivan</title>
</head>

<body style="background-color: rgb(211,235,243);">
    <header></header>

    <!--Nav principal con resize-->
    <nav class="nav-extended" style="background-color: rgb(255, 133, 0);">
        <div class="nav-wrapper">
            <a href="../index.html" class="brand-logo">Web Alex, Sara E Iván</a>
            <a href="#" data-target="slide-out" class="sidenav-trigger">
                <i class="material-icons">Menú</i>
            </a>
            <ul id="nav-mobile" class="right hide-on-med-and-down">
            <li><a href="../formularios/alta.html">Altas</a></li>
        <li><a href="../formularios/modificacion.html">Modificaciones</a></li>
        <li><a href="../formularios/consulta.html">Consultas</a></li>
        <li><a href="../formularios/baja.html">Bajas</a></li>
            </ul>
        </div>
    </nav>

    <!--Nav secundario mobile-->
    <ul class="sidenav" id="slide-out" style="transform: translateX(-105%);">
    <li><a href="../formularios/alta.html">Altas</a></li>
        <li><a href="../formularios/modificacion.html">Modificaciones</a></li>
        <li><a href="../formularios/consulta.html">Consultas</a></li>
        <li><a href="../formularios/baja.html">Bajas</a></li>
    </ul>

    <!--Main-->
    <main>
        <div class="container">
            <h4>Consultas</h4>
<?php
require_once '../clases/Conexion.php';
    try {
            $db = new PDO('mysql:host=' . $servidor . ';dbname=' . $bd, $usuario, $contraseña);
            if (!isset($_POST['Consultar'])) {
        }else {
            $nombre = $_POST['nombre'];
            //consulta para varios registros   
                $consulta4 = $db->prepare('SELECT dni,nombre,apellidos,email,fecha FROM clientes where nombre=?');
                $consulta4->execute(array($nombre));
                $registros = $consulta4->fetchAll();
                print "<BR> <br>TODOS LOS REGISTROS<br>"; 
                print'<table border="2">
                    <tr>
                    <th>DNI</th>
                    <th>Nombre</th>
                    <th>Apellidos</th>
                    <th>Email</th>
                    <th>Fecha</th>
                    </tr>';
                foreach($registros as $item)
                {
                    echo '<tr><td>'.$item[0] .'</td>'	;	
                    //ó $item['id']
                    echo '<td>'.$item[1] .'</td>'	;//nombre
                    echo '<td>'.$item[2] .'</td>'; //apellido
                    echo '<td>'.$item[3] .'</td>';//email
                    echo '<td>'.$item[4] .'</td></tr>'; //fecha
                    //ó $item['precio']
                    
                    
                }
            print'</table>';
            print '<input >';
        }
    } catch (PDOException $e) {

        echo 'Error conectando con la base de datos: ' . $e->getMessage();
    }
?>
 </div>
    </main>

    <!--Footer-->
    <footer class="page-footer" style="background-color: rgb(0,191,171);">
        <div class="container">
            <div class="row">
                <div class="col l6 s12">
                    <h5 class="black-text">Créditos</h5>
                    <p class="grey-text text-lighten-4">Realizado con un modelo kanban board en trello, 3 repositorios privados para cada dev y 1 master.</p>
                </div>
            </div>
        </div>
    </footer>
    <!-- Materialize JS -->
    <script src="../materialize/materialize.min.js"></script>
    <script>
        document.addEventListener('DOMContentLoaded', function() {
            var elems = document.querySelectorAll('.sidenav');
            var instances = M.Sidenav.init(elems);
        });
    </script>
</body>

</html>